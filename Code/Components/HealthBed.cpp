#include "HealthBed.h"

namespace
{
	void RegisterHealthBedComponent(Schematyc::IEnvRegistrar& r)
	{
		Schematyc::CEnvRegistrationScope c = r.Scope(IEntity::GetEntityScopeGUID());
		{
			Schematyc::CEnvRegistrationScope sC = c.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CHealthBedComponent));
		}
	}

	CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterHealthBedComponent);
}

void CHealthBedComponent::ReflectType(Schematyc::CTypeDesc<CHealthBedComponent>& d)
{
	d.SetGUID("{7C392842-310E-4208-9AB0-5F4337F63996}"_cry_guid);
	d.SetEditorCategory("Health");
	d.SetLabel("Health Bed");
}

void CHealthBedComponent::OnActorEntered(IStatusActor* a)
{
	IStatusActor::SActorStatus enteredActorStatus = a->GetActorStatus();
	CryLog("Actor entered the bed with health %f\nTemperature %f", (float)enteredActorStatus.m_actualHealth, (float)enteredActorStatus.m_actualInternalTemp);
	m_pActor = a;
	ParseActorStatus(a->GetActorStatus());
}

void CHealthBedComponent::GetDetailedInfection(Infection::IPathogen p)
{
	switch (p.m_pathogenType)
	{
	case Infection::EPathogenType::BACTERIAL:
		ParseBacterialInfection(m_pActor->GetBacterialInfection());
		break;
	case Infection::EPathogenType::VIRAL:
		break;
	case Infection::EPathogenType::FUNGAL:
		break;
	case Infection::EPathogenType::PRION:
		break;
	default:
		break;
	}
}

void CHealthBedComponent::DisplayHealthInfo(SHealthDisplayInfo i)
{
	IPersistantDebug* pDebug = gEnv->pGameFramework->GetIPersistantDebug();
	if (pDebug)
	{
		pDebug->Begin("display", false);
		pDebug->AddText(150, 150, 8.f, ColorF(Vec3(1, 1, 0), .25f), 10.f, "%s\nInfected: %s", i.m_genericText, i.m_bIsInfected ? "true" : "false");
		if (i.m_bIsInfected)
		{
			pDebug->AddText(50, 50, 4.f, ColorF(Vec3(1, 0, 1), .25f), 10.f, "Press 0 to perform detailed immunoassay");
		}
	}
}

void CHealthBedComponent::ParseActorStatus(IStatusActor::SActorStatus s)
{
	SHealthDisplayInfo dispInfo;
	string summary;
	// Can't be healthy and any of other statuses
	if (s.m_actualHealth == s.m_targetHealth) summary = "Healthy";
	if (s.m_infectionStatus != IStatusActor::EInfectionStatus::NOT_INFECTED) summary = "Infected\n";
	if (s.m_actualHealth != s.m_targetHealth) summary += "Injured\n";
	if (s.m_actualInternalTemp < s.m_targetInternalTemp) summary += "Hypothermic\n";
	if (s.m_actualInternalTemp > s.m_targetInternalTemp) summary += "Hyperthermic\n";
	dispInfo.m_genericText = summary;
	dispInfo.m_bIsInfected = s.m_infectionStatus != IStatusActor::EInfectionStatus::NOT_INFECTED;

	DisplayHealthInfo(dispInfo);
}

void CHealthBedComponent::ParseBacterialInfection(Infection::IBacteria b)
{
	string effectiveMeds;
	switch (b.m_effectiveClass)
	{
	case Medication::EAntibioticClass::NONE:
		effectiveMeds = "None";
		break;
	case Medication::EAntibioticClass::AMINOGLYCOSIDE:
		effectiveMeds = "Aminoglycosides";
		break;
	case Medication::EAntibioticClass::CARBAPENEM:
		effectiveMeds = "Carbapenems";
		break;
	case Medication::EAntibioticClass::CEPHALOSPORIN:
		effectiveMeds = "Cephalosporins";
		break;
	case Medication::EAntibioticClass::GLYCOPEPTIDE:
		effectiveMeds = "Glycopeptides";
		break;
	case Medication::EAntibioticClass::LINOMYCIN:
		effectiveMeds = "Linomycins";
		break;
	case Medication::EAntibioticClass::MACROLIDE:
		effectiveMeds = "Macrolides";
		break;
	case Medication::EAntibioticClass::PENICILLIN:
		effectiveMeds = "Penicillins";
		break;
	case Medication::EAntibioticClass::QUINOLONE:
		effectiveMeds = "Quinolones";
		break;
	case Medication::EAntibioticClass::SULFANOMIDE:
		effectiveMeds = "Sulfanomides";
		break;
	case Medication::EAntibioticClass::TETRACYCLINE:
		effectiveMeds = "Tetracyclines";
		break;
	default:
		effectiveMeds = "None";
		break;
	}
	string summary = string();
	summary.Format("Infection type: Bacterial\nSpecies name: %s\nIs Gram %s\nIs motile: %s\nRecommended medication: %s",
		b.m_name, b.m_bIsGramPositive ? "positive" : "negative", b.m_bIsMotile ? "yes" : "no", effectiveMeds);
}

void CHealthBedComponent::ParseViralInfection(Infection::IVirus v)
{
	string genomeType;
	switch (v.m_genomeType)
	{
	case Infection::EViralGenomeType::DNA_DOUBLE_STRANDED:
		genomeType = "Double Stranded DNA genome";
		break;
	case Infection::EViralGenomeType::DNA_SINGLE_STRANDED:
		genomeType = "Single Stranded DNA genome";
		break;
	case Infection::EViralGenomeType::RNA_DOUBLE_STRANDED:
		genomeType = "Double Stranded RNA genome";
		break;
	case Infection::EViralGenomeType::RNA_SINGLE_STRANDED:
		genomeType = "Single Stranded RNA genome";
		break;
	default:
		break;
	}

	string summary = string();
	summary.Format("Infection type: Viral\nBudding: %s\nGenome type: %s");
	IPersistantDebug* pDebug = gEnv->pGameFramework->GetIPersistantDebug();
	pDebug->Begin("virus", false);
	pDebug->AddText(300.f, 300.f, 4.f, ColorF(Vec3(0, 1, 1), .25), 10.f, summary);
}
