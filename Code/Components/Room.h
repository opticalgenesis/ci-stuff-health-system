#pragma once

#include "StdAfx.h"

#include "Infection.h"
#include "IActorSystem.h"
#include "Player.h"

#include <CryCore/StaticInstanceList.h>
#include <CryEntitySystem/IEntityComponent.h>
#include <CrySchematyc/CoreAPI.h>

class CRoomComponent final
	: public IEntityComponent
{
public:
	static void ReflectType(Schematyc::CTypeDesc<CRoomComponent>& d);

	virtual void Initialize() override;
	Cry::Entity::EventFlags GetEventMask() const override;
	virtual void ProcessEvent(const SEntityEvent& s) override;

	Infection::IBacteria m_bacteria;
	Infection::IVirus m_virus;
};