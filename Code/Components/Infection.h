#pragma once

#include "StdAfx.h"

#include <CrySchematyc/CoreAPI.h>
namespace Medication
{
	struct IIngredient
	{
		inline bool operator==(const IIngredient& r) const
		{
			return 0 == memcmp(this, &r, sizeof(r));
		}

		inline bool operator!=(const IIngredient& r) const
		{
			return 0 != memcmp(this, &r, sizeof(r));
		}

		Schematyc::CSharedString m_name;
		float m_price;

		static void ReflectType(Schematyc::CTypeDesc<IIngredient>& d);

		void Serialize(Serialization::IArchive& a)
		{
			a(m_name, "name", "Name");
			a.doc("name");
			a(m_price, "price", "Price");
			a.doc("price");
		}
	};

	// Which aspects of the bacterium the antibiotic targets
	enum class EBacterialWeaknesses : uint8
	{
		GRAM_POSITIVE = 1 << 0,
		GRAM_NEGATIVE = 1 << 1,
		MOTILE = 1 << 2,
		NON_MOTILE = 1 << 3,
		REPRODUCTIVE_CYCLE = 1 << 4,
		NONE = 1 << 5
	};

	// If the cure works immediately or not
	enum class EEffectiveTimeSpan : uint8
	{
		INSTANT = 1 << 0,
		ONE_DAY = 1 << 1
	};

	// The 10 classes of antibiotics
	enum class EAntibioticClass : uint16
	{
		PENICILLIN = 1 << 0,
		TETRACYCLINE = 1 << 1,
		CEPHALOSPORIN = 1 << 2,
		QUINOLONE = 1 << 3,
		LINOMYCIN = 1 << 4,
		MACROLIDE = 1 << 5,
		SULFANOMIDE = 1 << 6,
		GLYCOPEPTIDE = 1 << 7,
		AMINOGLYCOSIDE = 1 << 8,
		CARBAPENEM = 1 << 9,
		NONE = 1 << 10
	};

	static void ReflectType(Schematyc::CTypeDesc<EBacterialWeaknesses>& d)
	{
		d.SetGUID("{C7E2B215-6FA3-4E02-8BFE-A780E2E54A5C}"_cry_guid);
		d.SetLabel("Weaknesses");
		d.SetDescription("What the antibiotic targets");
		d.SetDefaultValue(EBacterialWeaknesses::NONE);
		d.AddConstant(EBacterialWeaknesses::NONE, "None", "Default");
		d.AddConstant(EBacterialWeaknesses::GRAM_NEGATIVE, "GN", "Gram Negative");
		d.AddConstant(EBacterialWeaknesses::GRAM_POSITIVE, "GP", "Gram Positive");
		d.AddConstant(EBacterialWeaknesses::MOTILE, "Motile", "Motile");
		d.AddConstant(EBacterialWeaknesses::NON_MOTILE, "NMotile", "Non-motile");
		d.AddConstant(EBacterialWeaknesses::REPRODUCTIVE_CYCLE, "Repro", "Reproductive Cycle");
	}

	static void ReflectType(Schematyc::CTypeDesc<EEffectiveTimeSpan>& d)
	{
		d.SetGUID("{10F18F4B-1FE4-49FA-8C29-E1DB89CD0426}"_cry_guid);
		d.SetLabel("Effective time");
		d.SetDefaultValue(EEffectiveTimeSpan::INSTANT);
		d.AddConstant(EEffectiveTimeSpan::INSTANT, "Instant", "Instant");
		d.AddConstant(EEffectiveTimeSpan::ONE_DAY, "One Day", "One Day");
	}

	static void ReflectType(Schematyc::CTypeDesc<EAntibioticClass>& d)
	{
		d.SetGUID("{15F95960-8A11-45A7-8908-5A8720529814}"_cry_guid);
		d.SetLabel("Antibiotic class");
		// todo
		d.SetDefaultValue(EAntibioticClass::NONE);
	}

	struct IAntibiotic
	{
		inline bool operator==(const IAntibiotic& r) const
		{
			return 0 == memcmp(this, &r, sizeof(r));
		}

		inline bool operator!=(const IAntibiotic& r) const
		{
			return 0 != memcmp(this, &r, sizeof(r));
		}

		Schematyc::CSharedString m_name;
		EBacterialWeaknesses m_weaknessTarget;
		EEffectiveTimeSpan m_timespan;
		EAntibioticClass m_class;
		// Ingredients for the antibiotic
		Schematyc::CArray<IIngredient> m_ingredients;

		static void ReflectType(Schematyc::CTypeDesc<IAntibiotic>& d);

		void Serialize(Serialization::IArchive& a)
		{
			a(m_name, "name", "Name");
			a.doc("Name");
			a(m_weaknessTarget, "target", "Target");
			a.doc("Target");
			a(m_timespan, "timespan", "Timespan");
			a.doc("Timespan");
			a(m_class, "class", "Class");
			a.doc("Class");
			a(m_ingredients, "ings", "Ingredients");
			a.doc("Ingredients");
		}
	};
}

namespace Infection
{
	enum class EPathogenType : uint8
	{
		BACTERIAL = 1 << 0,
		VIRAL = 1 << 1,
		FUNGAL = 1 << 2,
		PRION = 1 << 3
	};

	static void ReflectType(Schematyc::CTypeDesc<EPathogenType>& d)
	{
		d.SetGUID("{F73BDA07-CEBA-4C15-B4F1-E14BD1A6EF38}"_cry_guid);
		d.SetDefaultValue(EPathogenType::BACTERIAL);
		d.AddConstant(EPathogenType::BACTERIAL, "Bacterial", "Bacterial");
		d.AddConstant(EPathogenType::VIRAL, "Viral", "Viral");
		d.AddConstant(EPathogenType::FUNGAL, "Fungal", "Fungal");
		d.AddConstant(EPathogenType::PRION, "Prion", "Prion");
	}

	struct IPathogen
	{
		inline bool operator==(const IPathogen& r) const
		{
			return 0 == memcmp(this, &r, sizeof(r));
		}

		inline bool operator!=(const IPathogen& r) const
		{
			return 0 != memcmp(this, &r, sizeof(r));
		}

		static void ReflectType(Schematyc::CTypeDesc<IPathogen>& d);

		Schematyc::CSharedString m_name;
		EPathogenType m_pathogenType;
	};

	struct IBacteria final
		: public IPathogen
	{
		inline bool operator==(const IBacteria& r) const
		{
			return 0 == memcmp(this, &r, sizeof(r));
		}

		inline bool operator!=(const IBacteria& r) const
		{
			return 0 != memcmp(this, &r, sizeof(r));
		}

		static void ReflectType(Schematyc::CTypeDesc<IBacteria>& d);

		// If the bacterium tests positive on the Gram stain
		bool m_bIsGramPositive;
		// If the bacterium is motile (such as c. diff)
		bool m_bIsMotile;
		// Classes of antibiotic not effective against bacterium
		Medication::EAntibioticClass m_resistantClass;
		// Class of antibiotic effective against this bacterium
		Medication::EAntibioticClass m_effectiveClass;
	};

	enum class EViralGenomeType : uint8
	{
		RNA_SINGLE_STRANDED = 1 << 0,
		RNA_DOUBLE_STRANDED = 1 << 1,
		DNA_SINGLE_STRANDED = 1 << 2,
		DNA_DOUBLE_STRANDED = 1 << 3
	};

	static void ReflectType(Schematyc::CTypeDesc<EViralGenomeType>& d)
	{
		d.SetGUID("{2422FD56-CBDB-4F3E-B99F-27872A8EC07D}"_cry_guid);
		d.SetLabel("Viral Genome");
		d.SetDefaultValue(EViralGenomeType::DNA_DOUBLE_STRANDED);
		d.AddConstant(EViralGenomeType::DNA_DOUBLE_STRANDED, "dsDNA", "dsDNA");
		d.AddConstant(EViralGenomeType::DNA_SINGLE_STRANDED, "ssDNA", "ssDNA");
		d.AddConstant(EViralGenomeType::RNA_DOUBLE_STRANDED, "dsRNA", "dsRNA");
		d.AddConstant(EViralGenomeType::RNA_SINGLE_STRANDED, "ssRNA", "ssRNA");
	}

	struct IVirus final
		: public IPathogen
	{
		inline bool operator==(const IVirus& r) const
		{
			return 0 == memcmp(this, &r, sizeof(r));
		}

		inline bool operator!=(const IVirus& r) const
		{
			return 0 != memcmp(this, &r, sizeof(r));
		}

		static void ReflectType(Schematyc::CTypeDesc<IVirus>& d);

		EViralGenomeType m_genomeType;
		bool m_bIsBudding;
	};
}


namespace Defaults
{
	struct SHydroxyPropyl
		: Medication::IIngredient
	{
		SHydroxyPropyl()
		{
			m_name = "Hydroxypropyl methylcellulose";
			m_price = 100.0f;
		}
	};

	struct SPenicillin
		: Medication::IAntibiotic
	{
		SPenicillin()
		{
			Schematyc::CArray<Medication::IIngredient> ings = Schematyc::CArray<Medication::IIngredient>();
			ings.PushBack(SHydroxyPropyl());
			m_name = "Penicillin";
			m_weaknessTarget = Medication::EBacterialWeaknesses::GRAM_POSITIVE;
			m_timespan = Medication::EEffectiveTimeSpan::ONE_DAY;
			m_class = Medication::EAntibioticClass::PENICILLIN;
			m_ingredients = ings;
		}
	};

	struct SStaphAureus final
		: public Infection::IBacteria
	{
		SStaphAureus()
		{
			m_name = "Staphylococcus Aureus";
			m_bIsGramPositive = true;
			m_bIsMotile = false;
			// Examples such as methicillin could go here
			m_resistantClass = Medication::EAntibioticClass::PENICILLIN;
		}
	};
}