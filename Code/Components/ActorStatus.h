#pragma once

#include "StdAfx.h"
#include "Infection.h"

#include <CryCore/StaticInstanceList.h>
#include <CryEntitySystem/IEntityComponent.h>
#include <CrySchematyc/CoreAPI.h>

struct IStatusActor
{
public:
	inline bool operator==(const IStatusActor& r) const
	{
		return 0 == memcmp(this, &r, sizeof(r));
	}

	inline bool operator!=(const IStatusActor& r) const
	{
		return 0 != memcmp(this, &r, sizeof(r));
	}

	static void ReflectType(Schematyc::CTypeDesc<IStatusActor>& r);

public:
	// Describes the infection status of the actor
	enum class EInfectionStatus : uint8
	{
		NOT_INFECTED = 1 << 0,
		BACTERIAL = 1 << 1,
		VIRAL = 1 << 2,
		PRION = 1 << 3,
		FUNGAL = 1 << 4
	};

	static void ReflectType(Schematyc::CTypeDesc<EInfectionStatus>& d);

	struct SActorStatus
	{
		inline bool operator==(const IStatusActor& r) const
		{
			return 0 == memcmp(this, &r, sizeof(r));
		}

		inline bool operator!=(const IStatusActor& r) const
		{
			return 0 != memcmp(this, &r, sizeof(r));
		}

		static void ReflectType(Schematyc::CTypeDesc<IStatusActor>& r);

		float m_targetHealth = 100.f;
		float m_actualHealth = m_targetHealth;
		float m_targetInternalTemp = 37.8f;
		float m_actualInternalTemp = m_targetInternalTemp;
		float m_minTemp = 5.f;
		float m_maxTemp = 45.f;

		EInfectionStatus m_infectionStatus = EInfectionStatus::NOT_INFECTED;
	};

	// Set this when the actor actually gets infected
	Infection::IBacteria m_bacterialInfection;
	Infection::IVirus m_viralInfection;

	virtual SActorStatus GetActorStatus() = 0;
	virtual void SetActorStatus(SActorStatus) = 0;

	virtual Infection::IBacteria GetBacterialInfection() = 0;
	virtual void SetBacterialInfection(Infection::IBacteria) = 0;

	virtual Infection::IVirus GetViralInfetion() = 0;
	virtual void SetViralInfection(Infection::IVirus) = 0;

	virtual void SetCured() = 0;
};