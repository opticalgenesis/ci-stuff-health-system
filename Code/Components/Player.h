#pragma once

#include "ActorStatus.h"
#include "HealthBed.h"

#include <thread>
#include <chrono>
#include <future>

#include <CryEntitySystem/IEntityComponent.h>
#include <CryMath/Cry_Camera.h>
#include <CryPhysics/physinterface.h>

#include <CryThreading/IThreadManager.h>

#include <ICryMannequin.h>
#include <CrySchematyc/Utils/EnumFlags.h>

#include <DefaultComponents/Cameras/CameraComponent.h>
#include <DefaultComponents/Input/InputComponent.h>

////////////////////////////////////////////////////////
// Represents a player participating in gameplay
////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// All enums are typed to either uint8 or uint16 to allow for
// NetSerialization using either 'ui8' or 'ui16' compression policy
////////////////////////////////////////////////////////////////////
class CPlayerComponent final 
	: public IEntityComponent
	, public IStatusActor
{
	enum class EInputFlagType
	{
		Hold = 0,
		Toggle
	};

	enum class EInputFlag : uint8
	{
		// 00000001
		MoveLeft = 1 << 0,
		// 00000010
		MoveRight = 1 << 1,
		// 00000100
		MoveForward = 1 << 2,
		// 00001000
		MoveBack = 1 << 3
	};

	static constexpr EEntityAspects InputAspect = eEA_GameClientD;
	
public:
	CPlayerComponent() = default;
	virtual ~CPlayerComponent() = default;

	// IEntityComponent
	virtual void Initialize() override;

	// Sets which events to listen to
	virtual Cry::Entity::EventFlags GetEventMask() const override;
	// Process the registered events
	virtual void ProcessEvent(const SEntityEvent& event) override;
	
	struct SRayCastThread
		: IThread
	{
		SRayCastThread(std::function<void()> f)
			: m_bShouldStop(false)
			, m_func(f) {}

		virtual void ThreadEntry() override
		{
			while (!m_bShouldStop)
			{
			}
		}

		void SignalStop() { m_bShouldStop = true; }
		volatile bool m_bShouldStop;
		std::function<void()> m_func;
	};

	virtual bool NetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags) override;
	virtual NetworkAspectType GetNetSerializeAspectMask() const override { return InputAspect; }
	// ~IEntityComponent

	// IStatusActor
	virtual SActorStatus GetActorStatus() override { return m_actorStatus; }
	virtual void SetActorStatus(SActorStatus s) override { m_actorStatus = s; }

	virtual Infection::IBacteria GetBacterialInfection() override;
	virtual void SetBacterialInfection(Infection::IBacteria b) override;
	virtual Infection::IVirus GetViralInfetion() override;
	virtual void SetViralInfection(Infection::IVirus v) override;
	virtual void SetCured() { return; }

	// Reflect type to set a unique identifier for this component
	static void ReflectType(Schematyc::CTypeDesc<CPlayerComponent>& desc)
	{
		desc.SetGUID("{63F4C0C6-32AF-4ACB-8FB0-57D45DD14725}"_cry_guid);
	}

	void OnReadyForGameplayOnServer();
	bool IsLocalClient() const { return (m_pEntity->GetFlags() & ENTITY_FLAG_LOCAL_PLAYER) != 0; }

	void CastRay();

	SRayCastThread* m_pRayCastThr = nullptr;
	std::thread* m_pStdThread = nullptr;
	
protected:
	void Revive(const Matrix34& transform);
	void HandleInputFlagChange(CEnumFlags<EInputFlag> flags, CEnumFlags<EActionActivationMode> activationMode, EInputFlagType type = EInputFlagType::Hold);

	// Called when this entity becomes the local player, to create client specific setup such as the Camera
	void InitializeLocalPlayer();
	
	// Start remote method declarations
protected:
	// Parameters to be passed to the RemoteReviveOnClient function
	struct RemoteReviveParams
	{
		// Called once on the server to serialize data to the other clients
		// Then called once on the other side to deserialize
		void SerializeWith(TSerialize ser)
		{
			// Serialize the position with the 'wrld' compression policy
			ser.Value("pos", position, 'wrld');
			// Serialize the rotation with the 'ori0' compression policy
			ser.Value("rot", rotation, 'ori0');
		}
		
		Vec3 position;
		Quat rotation;
	};

	// Remote method intended to be called on all remote clients when a player spawns on the server
	bool RemoteReviveOnClient(RemoteReviveParams&& params, INetChannel* pNetChannel);
	
protected:
	bool m_isAlive = false;

	Cry::DefaultComponents::CCameraComponent* m_pCameraComponent = nullptr;
	Cry::DefaultComponents::CInputComponent* m_pInputComponent = nullptr;

	CEnumFlags<EInputFlag> m_inputFlags;
	Vec2 m_mouseDeltaRotation;

private:
	SActorStatus m_actorStatus = SActorStatus();
	CHealthBedComponent* m_pBedComponent;
	bool m_bShouldCastRay = true;
};
